from os import environ

from flask import Flask, jsonify

port = environ.get("PORT", 5000)
app = Flask(__name__)


@app.get("/")
def hello_world():
    return jsonify({"message": "Hello, World!"})
